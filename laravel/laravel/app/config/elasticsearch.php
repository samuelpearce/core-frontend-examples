<?php

use Monolog\Logger;

return array(
    'hosts' => array(
					'localhost:9200',
					'kmi-web29:9202',
					'kmi-web29:9203',
					'kmi-web29:9204',
					'kmi-web08:9204',
					'kmi-web08:9200'
                    ),
    'logPath' => './es.log',
    'logLevel' => Logger::INFO
);