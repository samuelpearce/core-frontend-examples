<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoreController extends Controller
{
	/**
	 * @Route("/hekllo/{name}", name="_demo_hello")
	 * @Template()
	 */
    public function helloAction()
    {
        return $this->render('CoreWebBundle:Welcome:index.html.twig');
    }
}
