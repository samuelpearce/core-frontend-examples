## Laravel ##

\+ Clear documentation  
\+ Best routing (Less code to do same functionality as other frameworks)  
\+ Convention based (possible loosing control compared to symfony but for a website, its good)  
\+ Seems very well suited to use as an API (Automatic JSON methods + HTTP method controls)  


### Example routing:  ##
* All requests are served by public/index.php  
* Define your Route in app/routes.php  
* This can call a controller in app/controllers/  
* models are stored in app/models/  
* views can be written in blade templating system found in app/views/. Views with .blade.php extention are automatically detected and parsed.  

## Phalcon ##
\+ Fast to execute  
\+ Documentation is clear (I prefer laravel format though)  
\+ Because framework is compiled, the project file only contains your code  
\+ Documentation seems to include more configuration options  
\- Complicated to set up (requires compiling C module and including it in php.ini for each development and production enviroment)  

### Example routing: ###
* All requests are served by public/index.php  
* Routing can me more advanced and complex than laravel. But the default behaviour is /controler/action/params. see  http://docs.phalconphp.com/en/latest/reference/routing.html#default-behavior  
* Controllers are placed in app/controllers  
* Models are stored in app/models/  
* Views can we written using Volt. These are compiled into php files at runtime and cached. It uses a similar syntax to JSP.  

## Symfony ##
\+ Builtin profiler (shows statistics on php speed, request headers, session details etc)  
\- Seemed harder to get started (creating a 'bundle' package and adding it to the configuration. Other frameworks seem to be able to dynamically detect new packages/files)  

### Example routing: ###
* Routes can be placed in the controller classes. This needs to be configured in a yaml file. This is stored in a Bundle.  
* Routing: see http://symfony.com/doc/current/book/routing.html - It is configured in a yaml/xml or php file in 'app/config/routing.(yml|xml|php)'.  
* Bundles need to be manually registered in App/appKernel.php  
* All requests are served by web/app.php (or web/app_dev.php)  
* Controllers are placed in src\Core\WebBundle\Controller\  
* Models can be stored wherever - http://www.ricardclau.com/2012/02/where-is-the-model-in-symfony2-entities-repositories-and-services/ 
* Views are written using Twig - src\Core\WebBundle\Resources\views